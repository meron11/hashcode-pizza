﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Slice
    {
        public List<Cell> ReservedForThisSlice { get; set; }
        private static int Color { get; set; } = 15;

        public bool IsRectangle(int maxCells, int minimumIngr)
        {
            List<int> columnsNumbers = new List<int>();
            List<int> rowNumbers = new List<int>();

            rowNumbers.Add(ReservedForThisSlice[0].Row);
            columnsNumbers.Add(ReservedForThisSlice[0].Column);

            for(int i = 1; i < ReservedForThisSlice.Count; i++)
            {
                if (!rowNumbers.Contains(ReservedForThisSlice[i].Row))
                    rowNumbers.Add(ReservedForThisSlice[i].Row);

                if (!columnsNumbers.Contains(ReservedForThisSlice[i].Column))
                    columnsNumbers.Add(ReservedForThisSlice[i].Column);
            }
            var ts = ReservedForThisSlice.FindAll(c => c.C is Ingredient.T);

            return (maxCells >= columnsNumbers.Count * rowNumbers.Count) && 
                    ts.Count >= minimumIngr && 
                    ts.Count - ReservedForThisSlice.Count >= minimumIngr;
        }

        public bool IsReservedForThisSlice(Cell c)
        {
            return ReservedForThisSlice.Contains(c);
        }

        public void Reserve(Pizza p)
        {
            Color = Color % 16 + 1;
            foreach (Cell c in ReservedForThisSlice)
            {
                c.Reserved = true;
                c.Color = Color;
            }
        }
    }
}
