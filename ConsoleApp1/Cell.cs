﻿namespace ConsoleApp1
{
    public class Cell
    {
        public Ingredient C { get; set; }
        public bool Reserved { get; set; }
        public int SliceID { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }

        private int _color = 15;
        public int Color
        {
            get { return _color; }
            set
            {
                if (value % 16 == 0)
                    _color = 1;
                else
                    _color = value % 15;
            }
        } 
    }
}
