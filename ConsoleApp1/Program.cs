﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var example = PizzaReader.ReadFile("PizzaShop/example.in");
            var small = PizzaReader.ReadFile("PizzaShop/small.in");
            var medium = PizzaReader.ReadFile("PizzaShop/medium.in");

            sortMG(example);
            example.Print();
            Console.Read();
        }


        static void sortMG(Pizza pizza)
        {
            List<Cell> cells = pizza.Cells.OfType<Cell>().ToList();


            List<Slice> slices = new List<Slice>();

            foreach (var cell in cells)
            {
                if (isReservedInSlice(cell))
                    continue;

                IEnumerable<Cell> opposideIngrediends;
                double distanceToElement = 999999999;


                if (cell.C == Ingredient.M)
                    opposideIngrediends = cells.Where(x => x.C == Ingredient.T && x.Reserved == false);
                else
                    opposideIngrediends = cells.Where(x => x.C == Ingredient.M && x.Reserved == false);


                Cell nearestElemSuccConstr = null;

                
                foreach (var opo in opposideIngrediends)
                {
                    var distance = Math.Sqrt(Math.Pow((cell.Column - opo.Column), 2) +
                                             Math.Pow((cell.Row - opo.Row), 2));
                    
                    
                    
                    
//                    var rectElems = cells.Where(x =>
//                        (x.Column >= cell.Column && x.Column <= nearestElemSuccConstr.Column)
//                        &&
//                        (x.Row >= cell.Row && x.Row <= nearestElemSuccConstr.Row)).ToList();
//
//                    var countOfTomatoes = rectElems.Count(x => x.C == Ingredient.T);
//                    var countOfMushrooms = rectElems.Count(x => x.C == Ingredient.M);
//
//                    
//                    if (countOfMushrooms >= pizza.MinEachIngredientPerSlice && countOfTomatoes >= pizza.MinEachIngredientPerSlice)
//                    {
                        if (distance < distanceToElement && distance > 0)
                        {
                            distanceToElement = distance;
                            nearestElemSuccConstr = opo;
                        }

//                    }
                }

                if (nearestElemSuccConstr != null)
                {
                    var rectElems = cells.Where(x =>
                        (x.Column >= cell.Column && x.Column <= nearestElemSuccConstr.Column)
                        &&
                        (x.Row >= cell.Row && x.Row <= nearestElemSuccConstr.Row)).ToList();

                    rectElems.ForEach(x => x.Reserved = true);

                    var slice = new Slice()
                    {
                        ReservedForThisSlice = rectElems
                    };

                    if (slice.ReservedForThisSlice.Count <= pizza.MaxCellsPerSlice &&
                        slice.ReservedForThisSlice.Count > 0)
                        slices.Add(slice);
                }

                foreach (Slice slc in slices)
                    slc.Reserve(pizza);
            }
            


            bool isReservedInSlice(Cell cell)
            {
                foreach (var slice in slices)
                {
                    if (slice.IsReservedForThisSlice(cell))
                        return true;
                }

                return false;
            }
        }
    }
}