﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class Pizza
    {
        public Cell[,] Cells { get; set; }
        public int MaxCellsPerSlice { get; set; }
        public int MinEachIngredientPerSlice { get; set; }
        public int NumberOfTomatoes { get; set; }
        public int NumberOfMushrooms { get; set; }

        public void Print()
        {
            if (Cells == null)
                return;

            for (int i = 0; i < Cells.GetLength(0); i++)
            {
                for (int j = 0; j < Cells.GetLength(1); j++)
                {
                    Console.ForegroundColor = (ConsoleColor)Cells[i, j].Color;
                    Console.Write(FromIngrToChar(Cells[i, j].C));
                }

                Console.WriteLine();
            }
        }

        private char FromIngrToChar(Ingredient i)
        {
            return i == Ingredient.M ? 'M' : 'T';
        }
    }
}
