﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1
{
    public static class PizzaReader
    {
        
        public static Pizza ReadFile(string path)
        {
            Pizza p = new Pizza();
            string[] ss = File.ReadAllLines(path);
            if (ss == null)
                return null;

            string[] temp;
            temp = ss[0].Split(' ');
            int r = Convert.ToInt32(temp[0]);
            int c = Convert.ToInt32(temp[1]);
            p.MinEachIngredientPerSlice = Convert.ToInt32(temp[2]);
            p.MaxCellsPerSlice = Convert.ToInt32(temp[3]);

            p.Cells = new Cell[r, c];
            for (int i = 1; i < ss.Length; i++)
            {
                string row = ss[i];
                for (int j = 0; j < c; j++)
                {
                    p.Cells[i - 1, j] = new Cell();
                    p.Cells[i - 1, j].C = FromCharToIngr((ss[i])[j]);
                    p.Cells[i - 1, j].Row = i - 1;
                    p.Cells[i - 1, j].Column = j;

                    if (p.Cells[i - 1, j].C is Ingredient.T)
                        p.NumberOfTomatoes++;
                    else
                        p.NumberOfMushrooms++;
                }
            }

            return p;
        }

        private static Ingredient FromCharToIngr(char c)
        {
            return c == 'T' ? Ingredient.T : Ingredient.M;
        }
    }
}
